MAIN_FILE := main.lua
START_DIRECTORY := src/
FILES := isExample.lua
all: build

build:
	node build.js $(START_DIRECTORY) $(MAIN_FILE) $(FILES)