// junk code but ok
const { writeFile, readFile } = require("fs/promises");
function createImportTable(list) {
    const lines = [];

    lines.push("local importTable = {");

    for (const listModule of list) {
        lines.push(`["${listModule.name}"] = function(...) ${listModule.value} end,`); // a bit janky but yea
    };

    lines.push("};\n");
    

    return lines.join("\n")
};

function build(source, files) {
    const newSource = `local loadModule;
local requireModule;
${createImportTable(files)}
loadModule = function(file) 
    return importTable[file];
end;

requireModule = function(file) 
    local module = loadModule(file);

    if module then return module() end;
end;

${source}
importTable = nil;`

    return newSource;
};

async function buildSource(startDirectory, sourceFileName, files) {
    try { 
        const importFiles = [];
        const sourceFile = await readFile(`${startDirectory}${sourceFileName}`)

        for (const file of files) {
            const sourceModule = await readFile(`${startDirectory}${file}`, "utf-8");
            
            importFiles.push({
                name: file,
                value: sourceModule
            });
        };

        await writeFile("./output.lua", build(sourceFile, importFiles));
    } catch (err) {
        console.error(err);
    };
};

process.argv.splice(0, 2);
const [ start, main, ...files ] = process.argv;

buildSource(start, main, files);