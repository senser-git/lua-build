local loadModule;
local requireModule;
local importTable = {
["isExample.lua"] = function(...) return { 
    example = true
}; end,
};

loadModule = function(file) 
    return importTable[file];
end;

requireModule = function(file) 
    local module = loadModule(file);

    if module then return module() end;
end;

print(requireModule("modules/isExample.lua").example);
importTable = nil;